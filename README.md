This repository holds the source code and documentation of the TRITEX
sequence assembly pipeline for Triticeae genomes developed in the 
research group Domestication Genomics at the Leibniz Institute of Plant 
Genetics and Crop Research (IPK) Gatersleben 
[link](https://www.ipk-gatersleben.de/index.php?id=1&L=1).

For more information, please refer to the [usage 
guide](https://tritexassembly.bitbucket.io).

In case of questions, contact [Martin 
Mascher](mailto:mascher@ipk-gatersleben.de).
