#!/bin/zsh

minia='/filer-dg/agruppen/seq_shared/mascher/source/minia/build500/bin/minia -out-compress 9 -debloom original'
samtools='/opt/Bio/samtools/1.9/bin/samtools'
n50='/filer-dg/agruppen/seq_shared/mascher/code_repositories/triticeae.bitbucket.io/shell/n50'

k=(-kmers 100,200,300,350,400,450,500)
m=(-mem 50000)
s=(-restart 0)

zparseopts -D -K -- -kmers:=k -reads:=r -mem:=m -threads:=t -outdir:=d -restart:=s

kmers=$k[2]
input=$r[2]
mem=$m[2]
threads=$t[2]
dir=$d[2]
restart=$s[2]

tr , '\n' <<< $kmers | read k 
prefix=$dir/minia_k${k}

if [[ $restart -eq 0 ]]; then

 if [[ -e $prefix ]]; then
  exit 1
 fi

 rm -rf $prefix
 mkdir -p $prefix

 wd=$PWD
 cd $prefix
 eval $minia -in $input -kmer-size $k -max-memory $mem \
  -out-dir $prefix -out $prefix:t -nb-cores $threads \
  > $prefix/log.out 2> $prefix/log.err 

 rm -f *glue* dummy* *h5
 fa=$prefix/$prefix:t.contigs.fa
 $samtools faidx $fa && $n50 $fa.fai > $fa.n50
 gzip $fa

 un=$prefix/$prefix:t.unitigs.fa
 $samtools faidx $un && $n50 $un.fai > $un.n50
 gzip $un

 cd $wd
else
 fa=$prefix/${prefix:t}.contigs.fa
fi

kk=$k

tr , '\n' <<< $kmers | tail -n +2 | while read k; do
 prefix=$dir/minia_k${k}

 if [[ -e $prefix ]]; then
  exit 1
 fi
 
 rm -rf $prefix
 mkdir -p $prefix

 list=$prefix/input_files.txt
 print -l $fa.gz $fa.gz $fa.gz $input > $list

 wd=$PWD
 cd $prefix
 eval $minia -in $list -kmer-size $k -max-memory $mem \
   -out-dir $prefix -out $prefix:t -nb-cores $threads \
   > $prefix/log.out 2> $prefix/log.err 

 rm -f *glue* dummy* *h5
 fa=$prefix/$prefix:t.contigs.fa
 $samtools faidx $fa && $n50 $fa.fai > $fa.n50
 gzip $fa

 un=$prefix/$prefix:t.unitigs.fa
 $samtools faidx $un && $n50 $un.fai > $un.n50
 gzip $un

 cd $wd
 
 kk=$k
done
