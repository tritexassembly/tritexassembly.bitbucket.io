#!/bin/zsh

# DEPRECATED, use digest_emboss.zsh. It is much faster.

bedtools='/opt/Bio/bedtools/2.26.0/bin/bedtools'
parallel='/opt/Bio/parallel/20160322/bin/parallel'

zparseopts -D -K -- -name:=n -site:=s -ref:=r -minlen:=m -cores:=c

ref=$r[2]
name=$n[2]
site=$s[2]
minlen=$m[2]
cores=$c[2]

cmd="'{
 len = length(site)
 l = 0
 s = \$2
 i = index(s, site)
 printf \$1
 while (i > 0) {
  printf \"\t\"l + i
  s = substr(s, i + len)
  l = l + i + len - 1
  i = index(s, site)
 }
 print \"\"
}'"

bed=${ref:r}_${name}_fragments_${minlen}bp.bed

{
 awk '/>/ {printf "\n"$1"\t"; next} {printf $0} END {print ""}' $ref | tr -d '>' | awk NF \
  | $parallel --line-buffer --will-cite -j $cores --pipe awk -v site=$site "$cmd" \
  | awk -v site=$site '{for(i = 2; i < NF; i++) print $1"\t"$i+length(site)-1"\t"$(i+1)-1}' \
  | sort -S10G -k 1,1 -k 2,2n \
  | awk '$3 - $2 >= '$minlen > $bed && \
 awk 'BEGIN{OFS="\t"}
      $3 - $2 <= 400 {print $0, $1":"$2"-"$3; next} 
      {print $1, $2, $2+200, $1":"$2"-"$3;
       print $1, $3-200, $3, $1":"$2"-"$3}' $bed \
  | $bedtools nuc -bed - -fi $ref > ${bed:r}_split.nuc.txt 
} 2> ${ref:r}_${name}_fragments_${minlen}bp_digest.err  
