#!/bin/zsh

# DEPRECATED, use BBmerge instead

p=(-pear '/opt/Bio/pear/0.9.6/bin/pear')
m=(-maxlen 550)

zparseopts -K -D -- -threads:=t -maxlen:=m -mem:=e -pear:=p
arg=$1

pear=$p[2]
threads=$t[2]
maxlen=$m[2]
mem=$e[2]

tr ':' '\t' <<< $arg | read i1 i2 base

{
 mktemp --suffix=.fastq ${base}_R1_tmp_XXXXXX | read r1
 mktemp --suffix=.fastq ${base}_R2_tmp_XXXXXX | read r2

 gzip -cd ${i1} > $r1 &
 gzip -cd ${i2} > $r2 &

 wait

 $pear -j $threads -y $mem \
  -f $r1 -r $r2 \
  -m $maxlen -n 250 -t 50 -v 10 -q 10 \
  -o ${base}_pear && rm -f $r1 $r2
} 2> ${base}_pear.err > ${base}_pear.out && \
 sed -n '2~4p' ${base}_pear.assembled.fastq | awk '{print length}' | sort -n | uniq -c \
  | awk '{print $2"\t"$1}' > ${base}_pear.assembled_length_dist.tsv
