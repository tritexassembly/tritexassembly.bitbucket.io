#!/bin/zsh

#DEPRECATED, use ihist.txt file created by BBMerge

gzip=$1
shift
i=$1

if [[ $gzip -eq 0 ]]; then
 cat='cat'
 base=$i:r
else
 cat='zcat'
 base=$i:r:r
fi

$cat $i | sed -n '2~4p' | awk '{print length}' | sort -n | uniq -c \
 | awk '{print $2"\t"$1}' > ${base}_length_dist.tsv 2> ${base}_length_dist.err 
