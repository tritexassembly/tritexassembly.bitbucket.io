#!/bin/zsh

minia='/filer-dg/agruppen/seq_shared/mascher/source/minia/build500/bin/minia -keep-isolated -out-compress 9 -debloom original'
prep='/filer-dg/agruppen/seq_shared/mascher/code_repositories/genome_assembly_pipeline/shell/prep_assembly.zsh'

k=(-kmers 100,200,300,350,400,450,500)
m=(-mem 50000)
s=(-restart 0)

zparseopts -D -K -- -kmers:=k -reads:=r -mem:=m -threads:=t -outdir:=d -restart:=s

kmers=$k[2]
input=$r[2]
mem=$m[2]
threads=$t[2]
dir=$d[2]
restart=$s[2]

tr , '\n' <<< $kmers | read k 
prefix=$dir/minia_k${k}

if [[ $restart -eq 0 ]]; then

 if [[ -e $prefix ]]; then
  exit 1
 fi

 rm -rf $prefix
 mkdir -p $prefix

 wd=$PWD
 cd $prefix
 eval $minia -in $input -kmer-size $k -max-memory $mem \
  -out-dir $prefix -out $prefix:t -nb-cores $threads \
  > $prefix/log.out 2> $prefix/log.err 

 rm -f *glue* dummy* *h5
 fa=$prefix/$prefix:t.contigs.fa
 $prep $fa 0 1 2> ${fa:r}_prep.err 

 un=$prefix/$prefix:t.unitigs.fa
 $prep $un 0 1 2> ${un:r}_prep.err 

 cd $wd
else
 fa=$prefix/${prefix:t}.contigs.fa.gz
fi

kk=$k

tr , '\n' <<< $kmers | tail -n +2 | while read k; do
 prefix=$dir/minia_k${k}

 if [[ -e $prefix ]]; then
  exit 1
 fi
 
 rm -rf $prefix
 mkdir -p $prefix

 list=$prefix/input_files.txt
 print -l $fa.gz $fa.gz $fa.gz $input > $list

 wd=$PWD
 cd $prefix
 eval $minia -in $list -kmer-size $k -max-memory $mem \
   -out-dir $prefix -out $prefix:t -nb-cores $threads \
   > $prefix/log.out 2> $prefix/log.err 

 rm -f *glue* dummy* *h5
 fa=$prefix/$prefix:t.contigs.fa
 $prep $fa 0 1 2> ${fa:r}_prep.err 

 un=$prefix/$prefix:t.unitigs.fa
 $prep $un 0 1 2> ${un:r}_prep.err 

 cd $wd
 
 kk=$k
done
