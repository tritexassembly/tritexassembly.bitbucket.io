#!/bin/awk -f

# DEPRECATED. Use `seqtk cutN -g` instead.

/^>/ {
 h=$1
 next
} 

{
 n=0
 for(i = 1; i <= NF; i++){
  if($i == "N" && n == 0){
   printf h"\t"i-1"\t"
   n=1
  } 
  else if($i == "N" && n != 0){
   n++
   if(i == NF)
    print i
  }
  else if($i != "N" && n != 0){
   print i-1
   n=0
  }
 }
}
